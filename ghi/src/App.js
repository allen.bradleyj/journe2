import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import FeedsList from "./components/feeds/FeedsList";
import FeedsListAll from "./components/feeds/FeedsListAll";
import FeedsForm from "./components/feeds/FeedsForm";
import FeedsUpdate from "./components/feeds/FeedsUpdate";
import MapPage from "./MapPage";
import UserForm from "./components/users/UserForm";
import UserList from "./components/users/UserList";
import LoginForm from "./LoginForm";
import Chat from "./components/feeds/Chat";
import AboutUs from "./components/AboutUs/AboutUs";
import UserProfile from "./components/users/UserProfile";

const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, '');

function App() {
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={
            <>
              <div>
                <MainPage />
              </div>
            </>
          } />
          <Route path="/feeds">
            <Route index element={<FeedsList />} />
            <Route path="new" element={
              <>
                <div>
                  <MapPage />
                  <FeedsForm />
                </div>
              </>
            } />
            <Route path="all" element={<FeedsListAll />} />
            <Route path="update/:id" element={<FeedsUpdate />} />
          </Route>
          <Route path="/users">
            <Route index element={<UserList />} />
            <Route path="signup" element={<UserForm />} />
          </Route>
          <Route path="/login">
            <Route path="" element={<LoginForm />} />
          </Route>
          <Route path="/chat">
            <Route path="" element={<Chat />} />
          </Route>
          <Route path="/aboutus">
            <Route index element={<AboutUs />} />
          </Route>
          <Route path="/profile">
            <Route index element={<UserProfile />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
