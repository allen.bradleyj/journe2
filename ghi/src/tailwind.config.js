module.exports = {
    content: ["./src/**/*.{js,jsx"],
    theme: {
        extend: {
            colors: {
                primary: "#0D47A1",
            }
        },
        fontFamily: {
            sans: ['Monserrat', 'sans-serif']
        }
    },
    plugins: [],
}