import React, { useEffect, useRef, useState } from 'react';

const Chat = () => {
    const messagesRef = useRef(null);
    const ws = useRef(null);
    const [submittedText, setSubmittedText] = useState('');
    const [messageList, setMessageList] = useState([]);

    useEffect(() => {
        const connectWebSocket = () => {
            ws.current = new WebSocket(`${process.env.REACT_APP_SAMPLE_SERVICE_WS_HOST}/ws`);

            ws.current.addEventListener('open', () => {
                console.log('WebSocket connection established.');
            });

            ws.current.addEventListener('message', event => {
                const receivedMessage = event.data;
                setMessageList(prevMessages => [...prevMessages, receivedMessage]);
            });
        };

        connectWebSocket();

        const handleSubmit = event => {
            event.preventDefault();
            const input = document.getElementById('messageText');
            const text = input.value;

            if (ws.current.readyState === WebSocket.OPEN) {
                ws.current.send(text);
                setSubmittedText(text);
            } else {
                console.log('WebSocket connection is not open.');
            }

            input.value = '';
        };

        const form = document.getElementById('message-form');
        form.addEventListener('submit', handleSubmit);

        return () => {
            form.removeEventListener('submit', handleSubmit);
        };
    }, []);

    return (
        <div>
            <h1>Chat</h1>
            <form id="message-form">
                <input type="text" id="messageText" autoComplete="off" />
                <button>Send</button>
            </form>
            <ul ref={messagesRef}>
                {messageList.map((message, index) => (
                    <li key={index}>{message}</li>
                ))}
            </ul>
            {submittedText && <p>{submittedText}</p>}
        </div>
    );
};

export default Chat;
