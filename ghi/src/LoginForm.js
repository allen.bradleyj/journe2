import { useState, useEffect } from "react";
import { setAuthToken } from "./components/auth/AuthToken";
import axios from 'axios';


const token = localStorage.getItem("token");

const LoginForm = () => {
	// eslint-disable-next-line
	const [loginTrue, setLoginTrue] = useState(false)
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [errorMessage, setErrorMessage] = useState('')

	const formData = new FormData();
	formData.append('username', username);
	formData.append('password', password);


	const handleSubmit = (e) => {
		e.preventDefault();
		setErrorMessage('')
		axios.post(`${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/token`, formData, {
			withCredentials: true,
		})
			.then(response => {
				const token = response.data.access_token;

				localStorage.setItem("token", token)

				setAuthToken(token)
				setLoginTrue(true)

			})
			.catch(err => { if (401 === err.response.status) { setErrorMessage("Incorrect username or password."); console.log("error!") }; })
		e.target.reset();
	};

	useEffect(() => {
		if (loginTrue === true) {

			setTimeout(() => {
				setLoginTrue(false)
				window.location.href = "/module3-project-gamma";
			}, 1000);
			return () => clearTimeout()
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [loginTrue]);

	return (
		<>
			{token && (
				<>
					<p>You are already logged in! Returning to the home screen!</p>
				</>
			)}

			{!token && (<>
				<div style={{ padding: "20px", backgroundColor: "rgba(255, 255, 255, 0.8)", borderRadius: "10px" }}>
					<h5 className="header">Login</h5>
					<div className="c-body">
						<form onSubmit={(e) => handleSubmit(e)}>
							<div className="mb-3">
								<label className="form-label">Username:</label>
								<input
									name="username"
									type="text"
									className="form-control"
									onChange={(e) => setUsername(e.target.value)}
								/>
							</div>
							<div className="mb-3">
								<label className="form-label">Password:</label>
								<input
									name="password"
									type="password"
									className="form-control"
									onChange={(e) => setPassword(e.target.value)}
								/>
							</div>
							<div>
								<input className="btn btn-primary" type="submit" value="Login" />
							</div>
							{errorMessage && (<p style={{ marginTop: "5px", color: "red" }}>{errorMessage}</p>)}
						</form>
					</div>
				</div>
			</>
			)}
		</>
	);
};

export default LoginForm;
