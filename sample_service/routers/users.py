from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from routers.authenticator import authenticator
from queries.users import UserQueries
from .models import (
    UserIn,
    UserOut,
    UsersOut,
    DuplicateUserError,
    UserForm,
    UserToken,
    HttpError,
)
from .s3 import generate_upload_url
from queries.pool import pool


router = APIRouter()


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    account: UserOut


class HttpError(BaseModel):
    detail: str


class DuplicateAccountError(ValueError):
    pass


class UserToken(Token):
    user: UserOut


@router.get("/s3Url")
async def get_url(url=None):
    if url is None:
        url = generate_upload_url()
    return url


@router.get("/token", response_model=UserToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:

    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.get("/api/protected", response_model=bool)
async def get_protected(
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/api/users", response_model=UsersOut)
def users_list(
    users: UserQueries = Depends(authenticator.get_current_account_data),
):
    return {
        "users": UserQueries.get_all_users(users["id"]),
    }


@router.get("/api/users/{user_id}", response_model=UserOut)
def get_user(
    user_id: int,
    response: Response,
    queries: UserQueries = Depends(authenticator.get_current_account_data),
):
    record = UserQueries.get_user(id, user_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.post("/api/users", response_model=UserToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    queries: UserQueries = Depends(),
):
    password = authenticator.hash_password(info.password)
    try:
        user = queries.create_user(info, password)
    except DuplicateUserError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a user with those credentials",
        )
    form = UserForm(username=info.username, password=info.password)

    token = await authenticator.login(response, request, form, queries)

    return UserToken(user=user, **token.dict())


@router.put("/api/users/{id}", response_model=UserOut)
def update_user(
    id: int,
    user_in: UserIn,
    response: Response,
    queries: UserQueries = Depends(),
):
    hash_password = authenticator.hash_password(user_in.password)
    user_in.password = hash_password
    record = UserQueries.update_user(id, id, user_in)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/users/{user_id}", response_model=bool)
def delete_user(user_id: int, queries: UserQueries = Depends()):
    queries.delete_user(user_id)
    return True
