from fastapi.testclient import TestClient
from queries.users import UserQueries
from main import app

client = TestClient(app)


class UserQueriesMock:
    def delete_user(self, user_id):
        return [
            {
                "id": 1,
                "first": "myla",
                "last": "test",
                "email": "test@test.com",
                "username": "testtest",
                "password": "test",
                "zodiac_sign": "Gemini",
            }
        ]


def test_delete_user():
    app.dependency_overrides[UserQueries] = UserQueriesMock

    response = client.delete("/api/users/1")

    assert response.status_code == 200
    assert response.json() == True

    app.dependency_overrides = {}
